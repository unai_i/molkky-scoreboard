/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.molkkyscoreboard.MolkkyScoreboard 1.0


Dialog {
    id: page
    parent: window
    DialogHeader {
        id: header
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        title: qsTr("Settings")
        acceptText: qsTr("Accept")
    }
    Column {
        id: column

        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: Theme.horizontalPageMargin
        spacing: Theme.paddingLarge
        ComboBox{
            id: combobox
            anchors.left: parent.left
            anchors.right: parent.right
            label: qsTr("Game variant")
            menu: ContextMenu{
                id: contextMenu
                Repeater {
                    model: MolkkyModel.gameVariants
                    MenuItem { text: modelData }
                }
            }

            description: descriptiveText(currentIndex)

            function descriptiveText(index)
            {
                if(index === 0)
                {
                    return qsTr("These are the official Mölkky rules.");
                }
                else if(index === 1)
                {
                    return qsTr("In this variant, if you miss 3 times in a row you don't lose.\n"+
                                "Instead, your score drops either to 0 if it was below or equal to half the target score "+
                                "else it drops to half the target score.");
                }
                else if(index === 2)
                {
                    return qsTr("This variant is the same as Permissive excepted that "+
                                "if you reach the same score as one of your contestants, "+
                                "his score drops the same way as if he had missed 3 times in a row.");
                }
                else
                {
                    return "";
                }
            }

            Component.onCompleted: {
                var currentVariant = MolkkyModel.gameVariant;
                var count = MolkkyModel.gameVariants.length;
                for(var i = 0; i < count; i++)
                {
                    if(MolkkyModel.gameVariants[i] === currentVariant)
                    {
                        currentIndex = i;
                        break;
                    }
                }
            }
        }

        Slider{
            id: slider
            height: parent.height * 0.1
            anchors.left: parent.left
            anchors.right: parent.right
            label: qsTr("Game target score")
            minimumValue: 12
            maximumValue: 100
            stepSize: 2
            valueText: value
            Component.onCompleted: {
                value = MolkkyModel.targetScore;
            }
        }
    }
    onDone: {
        if(result === DialogResult.Accepted){
            MolkkyModel.gameVariant = MolkkyModel.gameVariants[combobox.currentIndex];
            MolkkyModel.targetScore = slider.value;
        }
    }
}





