/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.1
import Sailfish.Silica 1.0
import harbour.molkkyscoreboard.MolkkyScoreboard 1.0

Page{
    id: page
    parent: window

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {

            MenuItem {
                text: qsTr("Reset game and players")
                onClicked: MolkkyModel.resetNewGame();
            }
            MenuItem {
                text: qsTr("Restart game")
                onClicked: MolkkyModel.startNewGame();
            }
            MenuItem {
                text: qsTr("Redo")
                onClicked: MolkkyModel.redo();
                visible: MolkkyModel.canRedo
            }
            MenuItem {
                text: qsTr("Undo")
                onClicked: MolkkyModel.undo();
                visible: MolkkyModel.canUndo
            }
        }

        Connections{
            target: MolkkyModel
            onGamePhaseChanged:
            {
                switch(MolkkyModel.gamePhase)
                {
                    case MolkkyModel.Setup:
                        pageStack.replace(Qt.resolvedUrl("SetupPage.qml"))
                        break;
                    case MolkkyModel.SkittlePlacement:
                        pageStack.replace(Qt.resolvedUrl("SkittlePlacementPage.qml"));
                        break;
                    case MolkkyModel.Finished:
                        pageStack.replace(Qt.resolvedUrl("FinishedPage.qml"));
                        break;
                    default:
                        break;
                }
            }
        }

        Column{
            anchors.fill: parent
            anchors.margins: Theme.horizontalPageMargin

            PageHeader {
                id: header
                title: qsTr("Playing")
            }
            SilicaListView{
                id: teamsView
                height: parent.height-parent.children[0].height-parent.children[2].height
                anchors.left: parent.left
                anchors.right: parent.right
                clip: true
                model: MolkkyModel.sortedPlayers
                currentIndex: MolkkyModel.currentPlayerIndex
                highlight: BackgroundItem{
                    highlighted: true
                }

                highlightFollowsCurrentItem: true
                header: Row{
                    width: teamsView.width
                    spacing: width*0.01
                    Label{
                        width: parent.width*0.5 - parent.spacing*2/3
                        text: qsTr("Name")
                    }
                    Label{
                        width: parent.width*0.25 - parent.spacing*2/3
                        text: qsTr("Score")
                    }
                    Label{
                        width: parent.width*0.25 - parent.spacing*2/3
                        text: qsTr("Missed")
                    }
                }
                delegate: Row{
                    id: listItemDelegate
                    width: teamsView.width
                    spacing: width*0.01
                    Text{
                        width: parent.width*0.495 - parent.spacing*2/3
                        color: parent.ListView.isCurrentItem ? Theme.highlightColor
                                                            : Theme.primaryColor
                        text: name
                        elide: Text.ElideRight
                    }
                    Text{
                        width: parent.width*0.245 - parent.spacing*2/3
                        color: parent.ListView.isCurrentItem ? Theme.highlightColor
                                                            : Theme.primaryColor
                        text: score
                        horizontalAlignment: Text.AlignRight
                    }
                    Text{
                        width: parent.width*0.245 - parent.spacing*2/3
                        color: parent.ListView.isCurrentItem ? Theme.highlightColor
                                                            : Theme.primaryColor
                        text: missed
                        horizontalAlignment: Text.AlignRight
                    }
                    ListView.onRemove: RemoveAnimation{
                        target: listItemDelegate
                        duration: 1000
                    }
                }
                VerticalScrollDecorator{

                }
            }

            Column{
                id: gameView
                height: childrenRect.height
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: height * 0.01
                Text{
                    id: gameText
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width*0.1
                    anchors.right: parent.right
                    height: paintedHeight
                    color: Theme.primaryColor
                    text: qsTr("It's %1's turn.\nCurrent score is %2.%3").arg(MolkkyModel.currentPlayerName).arg(MolkkyModel.currentPlayerScore).arg(MolkkyModel.currentPlayerMissed?qsTr("\nYou already missed %1 %2").arg(MolkkyModel.currentPlayerMissed).arg(MolkkyModel.currentPlayerMissed > 1?qsTr("times"):qsTr("time")):"")
                }
                Flow{
                    id: scoreButtons
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: childrenRect.height
                    spacing: width*0.01
                    Repeater{
                        model: 12
                        Button{
                            width: scoreButtons.width/3-scoreButtons.spacing*2/3
                            text: index+1
                            onClicked: MolkkyModel.markPlayerScore(index+1);
                        }
                    }
                    Button{
                        width: scoreButtons.width
                        text: qsTr("missed")
                        onClicked: MolkkyModel.markPlayerScore(0);
                    }
                }
            }
        }
    }
    Connections{
        target: MolkkyModel
        onGamePlayerCountChanged:{
            playerCountChangeNotification.playerName = name;
            playerCountChangeNotification.won = won;
            playerCountChangeNotification.animate();
        }
    }

    Item{
        id: playerCountChangeNotification
        property string playerName: ""
        property bool won: false
        anchors.fill: parent
        opacity: 0
        MouseArea{
            anchors.fill: parent
            enabled: parent.opacity
        }

        Rectangle{
            anchors.centerIn: parent
            width: parent.width*0.8
            height: parent.height*0.4
            radius: Math.min(width, height)*0.2
            color: Qt.tint(Theme.primaryColor, "#b0000000")
            opacity: 0.8
            clip: true
            GlassItem{
                anchors.fill: parent
                color: Qt.tint(Theme.primaryColor, "#90000000")
                radius: Math.min(width, height)*0.2
                falloffRadius: Math.min(width, height)*0.002
                dimmed: true
                opacity: 1.0/parent.opacity
                Text{
                    anchors.centerIn: parent
                    width: parent.width-parent.radius*2
                    height: parent.height*0.5
                    color: Theme.primaryColor
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: qsTr("%1 has %2.").arg(playerCountChangeNotification.playerName).arg(playerCountChangeNotification.won?qsTr("won"):qsTr("lost"))
                }
            }
        }
        function animate(){
            itemAnimation.start();
        }
        SequentialAnimation{
            id: itemAnimation
            NumberAnimation{
                target: playerCountChangeNotification
                property: "opacity"
                from: 0
                to: 1
                duration: 500
            }
            NumberAnimation{
                target: playerCountChangeNotification
                property: "opacity"
                from: 1
                to: 1
                duration: 2000
            }
            NumberAnimation{
                target: playerCountChangeNotification
                property: "opacity"
                from: 1
                to: 0
                duration: 500
            }

        }
    }
}
