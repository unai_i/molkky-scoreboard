/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.molkkyscoreboard.MolkkyScoreboard 1.0

Dialog {
    parent: window
    onAccepted: MolkkyModel.placementFinished();
    acceptDestination: Qt.resolvedUrl("GamePage.qml")
    acceptDestinationAction: PageStackAction.Replace

    DialogHeader {
        id: header
        title: qsTr("Skittle placement");
        acceptText: qsTr("Placed")
    }
    Image{
        anchors.centerIn: parent
        source: "../images/molkkyGameStart.png"
        fillMode: Image.PreserveAspectFit
    }
}
