/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.molkkyscoreboard.MolkkyScoreboard 1.0

Page{
    id: page
    parent: window

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {

            MenuItem {
                text: qsTr("Change players")
                onClicked: MolkkyModel.resetNewGame();
            }
            MenuItem {
                text: qsTr("Play again")
                onClicked: MolkkyModel.startNewGame();
            }
        }

        Connections{
            target: MolkkyModel
            onGamePhaseChanged:
            {
                switch(MolkkyModel.gamePhase)
                {
                case MolkkyModel.Setup:
                    pageStack.replace(Qt.resolvedUrl("SetupPage.qml"))
                    break;
                case MolkkyModel.SkittlePlacement:
                    pageStack.replace(Qt.resolvedUrl("SkittlePlacementPage.qml"));
                    break;
                default:
                    break;
                }
            }
        }
        Column {
            anchors.fill: parent
            anchors.margins: Theme.horizontalPageMargin
            spacing: height * 0.02

            PageHeader {
                id: header
                title: qsTr("Finished")
            }
            Label {
                id: winnerText
                property int winnerTeam: 0
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width*0.8
                text: MolkkyModel.winnerName?qsTr("The winner is: %1!").arg(MolkkyModel.winnerName):MolkkyModel.winnerName;
            }
            SilicaListView
            {
                id: list
                width: parent.width*0.8
                height: parent.height * 0.6
                anchors.horizontalCenter: parent.horizontalCenter
                model: MolkkyModel.sortedPlayers
                clip: true
                header: Row{
                    width: list.width
                    spacing: width * 0.05
                    Label{
                        width: parent.width*0.2
                        text: qsTr("Rank")
                    }
                    Label{
                        width: parent.width*0.8-parent.spacing
                        text: qsTr("Name")
                    }
                }
                delegate: Row{
                    width: list.width
                    spacing: width * 0.05
                    Label{
                        width: parent.width*0.2
                        text: rank
                    }
                    Label{
                        width: parent.width*0.8-parent.spacing
                        text: name
                    }
                }
                VerticalScrollDecorator{

                }
            }
        }
    }
}
