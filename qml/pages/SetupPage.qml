/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.1
import Sailfish.Silica 1.0
import harbour.molkkyscoreboard.MolkkyScoreboard 1.0


Dialog {
    id: page
    parent: window
    onAccepted: {
        MolkkyModel.setupFinished();
    }
    acceptDestination: Qt.resolvedUrl("SkittlePlacementPage.qml")
    acceptDestinationAction: PageStackAction.Replace
    canAccept: playerCountField.acceptableInput

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {

            MenuItem {
                text: qsTr("Settings")
                onClicked: pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
            }
        }

        DialogHeader {
            id: header
            title: qsTr("Game setup");
            acceptText: qsTr("Done")
        }


        Column {
            id: baseItem
            anchors.top: header.bottom
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: Theme.horizontalPageMargin
            property ListModel model: null
            signal validated
            spacing: Theme.paddingSmall

            TextField{
                id: playerCountField
                anchors.left: parent.left
                anchors.right: parent.right
                activeFocusOnTab: true
                inputMethodHints: Qt.ImhDigitsOnly
                validator: IntValidator{
                    bottom: 2
                }
                label: qsTr("How many players are there?")
                text: "2"

                onFocusChanged: {
                    if(focus)
                    {
                        selectAll();
                    }
                    else
                    {
                        deselect();
                        MolkkyModel.setPlayerCount(text);
                    }
                }

                EnterKey.enabled: text.length > 0 && acceptableInput
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: {
                    MolkkyModel.setPlayerCount(text);
                    teamsListView.currentIndex = 0;
                    teamsListView.currentItem.focus = true;
                }
            }

            TextSwitch {
                id: shuffleSwitch
                text: qsTr("Shuffle players")
                checked: MolkkyModel.shufflePlayers
                onCheckedChanged: {
                    MolkkyModel.shufflePlayers = checked;
                }
            }

            SilicaListView{
                id: teamsListView
                anchors.left: parent.left
                anchors.right: parent.right
                height: parent.height*0.8
                clip: true
                model: MolkkyModel.players
                header: Label {
                    width: teamsListView.width
                    font.bold: true
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("Names")
                }
                delegate: TextField{
                    text: name
                    width: teamsListView.width
                    activeFocusOnTab: true
                    horizontalAlignment: Text.AlignHCenter
                    onFocusChanged: {
                        if(focus)
                        {
                            selectAll();
                        }
                        else
                        {
                            deselect();
                            MolkkyModel.setPlayerName(index, text)
                        }
                    }
                    EnterKey.enabled: text.length > 0
                    EnterKey.iconSource: index < (teamsListView.count-1)?"image://theme/icon-m-enter-next":"image://theme/icon-m-enter-close"
                    EnterKey.onClicked: {
                        MolkkyModel.setPlayerName(index, text);
                        if(index < teamsListView.count-1)
                            nextItemInFocusChain().focus = true;
                        else
                            focus = false;
                    }
                }
                VerticalScrollDecorator{

                }
            }
        }
    }
}
