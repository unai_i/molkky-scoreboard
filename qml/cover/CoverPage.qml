/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.molkkyscoreboard.MolkkyScoreboard 1.0

CoverBackground {
    PageHeader{
        id: header
        title: "Molkky"
    }

    SilicaListView
    {
        id: listView
        anchors.fill: parent
        anchors.topMargin: header.height
        model: MolkkyModel.sortedPlayers
        header: Row{
            width: listView.width
            Label{
                width: parent.width*0.6
                text: qsTr("Name")
            }
            Label{
                text: MolkkyModel.gamePhase === MolkkyModel.Finished?qsTr("Rank"):qsTr("Score")
            }
        }
        delegate: Row{
            anchors.left: parent.left
            anchors.right: parent.right
            Label{
                width: parent.width*0.6
                text: name
            }
            Label{
                horizontalAlignment: Text.AlignRight
                text: MolkkyModel.gamePhase === MolkkyModel.Finished?rank:score
            }
        }
    }
}


