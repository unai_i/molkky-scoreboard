<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="40"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="43"/>
        <source>Rank</source>
        <translation>Puesto</translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="43"/>
        <source>Score</source>
        <translation>Tanteo</translation>
    </message>
</context>
<context>
    <name>FinishedPage</name>
    <message>
        <location filename="../qml/pages/FinishedPage.qml" line="36"/>
        <source>Change players</source>
        <translation>Cambiar los jugadores</translation>
    </message>
    <message>
        <location filename="../qml/pages/FinishedPage.qml" line="40"/>
        <source>Play again</source>
        <translation>Volver a jugar</translation>
    </message>
    <message>
        <location filename="../qml/pages/FinishedPage.qml" line="69"/>
        <source>Finished</source>
        <translation>Fin</translation>
    </message>
    <message>
        <location filename="../qml/pages/FinishedPage.qml" line="76"/>
        <source>The winner is: %1!</source>
        <translation>El ganador es: %1!</translation>
    </message>
    <message>
        <location filename="../qml/pages/FinishedPage.qml" line="91"/>
        <source>Rank</source>
        <translation>Puesto</translation>
    </message>
    <message>
        <location filename="../qml/pages/FinishedPage.qml" line="95"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
</context>
<context>
    <name>GamePage</name>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="36"/>
        <source>Reset game and players</source>
        <translation>Reiniciar juego y jugadores</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="40"/>
        <source>Restart game</source>
        <translation>Reinicial juego</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="44"/>
        <source>Redo</source>
        <translation>Hacer</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="49"/>
        <source>Undo</source>
        <translation>Deshacer</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="82"/>
        <source>Playing</source>
        <translation>Jugando</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="104"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="108"/>
        <source>Score</source>
        <translation>Tanteo</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="112"/>
        <source>Missed</source>
        <translation>Fallado</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="162"/>
        <source>It&apos;s %1&apos;s turn.
Current score is %2.%3</source>
        <translation>Le toca a %1.
Tu tanteo es %2.%3</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="162"/>
        <source>
You already missed %1 %2</source>
        <translation>
Ya has fallado %1 %2</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="162"/>
        <source>times</source>
        <translation>veces</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="162"/>
        <source>time</source>
        <translation>vez</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="182"/>
        <source>missed</source>
        <translation>fallado</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="231"/>
        <source>%1 has %2.</source>
        <translation>%1 a %2.</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="231"/>
        <source>won</source>
        <translation>ganado</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="231"/>
        <source>lost</source>
        <translation>perdido</translation>
    </message>
</context>
<context>
    <name>MolkkyModel</name>
    <message>
        <location filename="../src/MolkkyModel.cpp" line="33"/>
        <source>Official</source>
        <translation>Official</translation>
    </message>
    <message>
        <location filename="../src/MolkkyModel.cpp" line="34"/>
        <source>Permissive</source>
        <translation>Permisivo</translation>
    </message>
    <message>
        <location filename="../src/MolkkyModel.cpp" line="35"/>
        <source>Machiavellian</source>
        <translation>Machiavélico</translation>
    </message>
</context>
<context>
    <name>PlayersListModel</name>
    <message>
        <location filename="../src/PlayersListModel.cpp" line="36"/>
        <source>Player %1</source>
        <translation>Jugador %1</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="35"/>
        <source>Settings</source>
        <translation>Opciónes</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="36"/>
        <source>Accept</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="42"/>
        <source>Game variant</source>
        <translation>Variante de juego</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="57"/>
        <source>These are the official Mölkky rules.</source>
        <translation>Estas son las regals officiales del Mölkky.</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="61"/>
        <source>In this variant, if you miss 3 times in a row you don&apos;t lose.
Instead, your score drops either to 0 if it was below or equal to half the target score else it drops to half the target score.</source>
        <translation>En esta variante, si fallas 3 veces consecutivas no pierdes.
En vez, tu tanteo baja a 0 si era menor o igual a la mitad del tanteo final o a la midad del tanteo final si era mayor.</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="67"/>
        <source>This variant is the same as Permissive excepted that if you reach the same score as one of your contestants, his score drops the same way as if he had missed 3 times in a row.</source>
        <translation>Esta variante es la misma que la Permisiva excepto que si empatas con otro jugador, su tanteo baja como si hubiera fallado 3 veces consecutivas.</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="96"/>
        <source>Game target score</source>
        <translation>Tanteo final</translation>
    </message>
</context>
<context>
    <name>SetupPage</name>
    <message>
        <location filename="../qml/pages/SetupPage.qml" line="46"/>
        <source>Settings</source>
        <translation>Opciónes</translation>
    </message>
    <message>
        <location filename="../qml/pages/SetupPage.qml" line="62"/>
        <source>Game setup</source>
        <translation>Preparación del juego</translation>
    </message>
    <message>
        <location filename="../qml/pages/SetupPage.qml" line="63"/>
        <source>Done</source>
        <translation>Listo</translation>
    </message>
    <message>
        <location filename="../qml/pages/SetupPage.qml" line="73"/>
        <source>How many players are there?</source>
        <translation>¿Cuantos jugadores hay?</translation>
    </message>
    <message>
        <location filename="../qml/pages/SetupPage.qml" line="99"/>
        <source>Shuffle players</source>
        <translation>Mezclar jugadores</translation>
    </message>
    <message>
        <location filename="../qml/pages/SetupPage.qml" line="114"/>
        <source>Names</source>
        <translation>Nombres</translation>
    </message>
</context>
<context>
    <name>SkittlePlacementPage</name>
    <message>
        <location filename="../qml/pages/SkittlePlacementPage.qml" line="32"/>
        <source>Skittle placement</source>
        <translation>Posicionamiento de los bolos</translation>
    </message>
    <message>
        <location filename="../qml/pages/SkittlePlacementPage.qml" line="33"/>
        <source>Placed</source>
        <translation>Posicionados</translation>
    </message>
</context>
</TS>
