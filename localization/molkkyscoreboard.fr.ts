<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="40"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="43"/>
        <source>Rank</source>
        <translation>Rang</translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="43"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
</context>
<context>
    <name>FinishedPage</name>
    <message>
        <location filename="../qml/pages/FinishedPage.qml" line="36"/>
        <source>Change players</source>
        <translation>Changer les joueurs</translation>
    </message>
    <message>
        <location filename="../qml/pages/FinishedPage.qml" line="40"/>
        <source>Play again</source>
        <translation>Rejouer</translation>
    </message>
    <message>
        <location filename="../qml/pages/FinishedPage.qml" line="69"/>
        <source>Finished</source>
        <translation>Fin</translation>
    </message>
    <message>
        <location filename="../qml/pages/FinishedPage.qml" line="76"/>
        <source>The winner is: %1!</source>
        <translation>Le vainqueur est: %1!</translation>
    </message>
    <message>
        <location filename="../qml/pages/FinishedPage.qml" line="91"/>
        <source>Rank</source>
        <translation>Rang</translation>
    </message>
    <message>
        <location filename="../qml/pages/FinishedPage.qml" line="95"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
</context>
<context>
    <name>GamePage</name>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="36"/>
        <source>Reset game and players</source>
        <translation>Réinitialiser partie et joueurs</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="40"/>
        <source>Restart game</source>
        <translation>Réinitialiser la partie</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="44"/>
        <source>Redo</source>
        <translation>Refaire</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="49"/>
        <source>Undo</source>
        <translation>Défaire</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="82"/>
        <source>Playing</source>
        <translation>Jeu en cours</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="104"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="108"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="112"/>
        <source>Missed</source>
        <translation>Raté</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="162"/>
        <source>It&apos;s %1&apos;s turn.
Current score is %2.%3</source>
        <translation>C&apos;est au tour de %1.
Ton score est de %2.%3</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="162"/>
        <source>
You already missed %1 %2</source>
        <translation>
Tu as déjà raté %1 %2</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="162"/>
        <source>times</source>
        <translation>fois</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="162"/>
        <source>time</source>
        <translation>fois</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="182"/>
        <source>missed</source>
        <translation>raté</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="231"/>
        <source>%1 has %2.</source>
        <translation>%1 a %2.</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="231"/>
        <source>won</source>
        <translation>gagné</translation>
    </message>
    <message>
        <location filename="../qml/pages/GamePage.qml" line="231"/>
        <source>lost</source>
        <translation>perdu</translation>
    </message>
</context>
<context>
    <name>MolkkyModel</name>
    <message>
        <location filename="../src/MolkkyModel.cpp" line="33"/>
        <source>Official</source>
        <translation>Officiel</translation>
    </message>
    <message>
        <location filename="../src/MolkkyModel.cpp" line="34"/>
        <source>Permissive</source>
        <translation>Permissif</translation>
    </message>
    <message>
        <location filename="../src/MolkkyModel.cpp" line="35"/>
        <source>Machiavellian</source>
        <translation>Machiavélique</translation>
    </message>
</context>
<context>
    <name>PlayersListModel</name>
    <message>
        <location filename="../src/PlayersListModel.cpp" line="36"/>
        <source>Player %1</source>
        <translation>Joueur %1</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="35"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="36"/>
        <source>Accept</source>
        <translation>Accepter</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="42"/>
        <source>Game variant</source>
        <translation>Variante de jeu</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="57"/>
        <source>These are the official Mölkky rules.</source>
        <translation>Ce sont les règles officielles du Mölkky.</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="61"/>
        <source>In this variant, if you miss 3 times in a row you don&apos;t lose.
Instead, your score drops either to 0 if it was below or equal to half the target score else it drops to half the target score.</source>
        <translation>Dans cette variante, si vous ratez 3 fois à la suite vous ne perdez pas.
En revanche, votre score tombe à 0 s&apos;il était inférieur ou égal au score final sinon il tombe à la moitié du score final.</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="67"/>
        <source>This variant is the same as Permissive excepted that if you reach the same score as one of your contestants, his score drops the same way as if he had missed 3 times in a row.</source>
        <translation>Cette variante est identique à la Permissive excepté que si vous égalisez avec un autre joueur, so score baisse comme s&apos;il avait raté 3 fois à la suite.</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="96"/>
        <source>Game target score</source>
        <translation>Score final</translation>
    </message>
</context>
<context>
    <name>SetupPage</name>
    <message>
        <location filename="../qml/pages/SetupPage.qml" line="46"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/pages/SetupPage.qml" line="62"/>
        <source>Game setup</source>
        <translation>Préparation de la partie</translation>
    </message>
    <message>
        <location filename="../qml/pages/SetupPage.qml" line="63"/>
        <source>Done</source>
        <translation>Fait</translation>
    </message>
    <message>
        <location filename="../qml/pages/SetupPage.qml" line="73"/>
        <source>How many players are there?</source>
        <translation>Combien y a-t-il de joueurs?</translation>
    </message>
    <message>
        <location filename="../qml/pages/SetupPage.qml" line="99"/>
        <source>Shuffle players</source>
        <translation>Mélanger les joueurs</translation>
    </message>
    <message>
        <location filename="../qml/pages/SetupPage.qml" line="114"/>
        <source>Names</source>
        <translation>Noms</translation>
    </message>
</context>
<context>
    <name>SkittlePlacementPage</name>
    <message>
        <location filename="../qml/pages/SkittlePlacementPage.qml" line="32"/>
        <source>Skittle placement</source>
        <translation>Placement des quilles</translation>
    </message>
    <message>
        <location filename="../qml/pages/SkittlePlacementPage.qml" line="33"/>
        <source>Placed</source>
        <translation>Placées</translation>
    </message>
</context>
</TS>
