/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Player.h"

Player::Player(QString name)
    : m_name(name), m_score(0), m_missed(0), m_rank(0), m_won(false)
{
}

QString Player::name() const
{
    return m_name;
}

void Player::setName(const QString &name)
{
    m_name = name;
}

int Player::score() const
{
    return m_score;
}

void Player::setScore(const int &score)
{
    m_score = score;
}

int Player::missed() const
{
    return m_missed;
}

void Player::setMissed(const int &missed)
{
    m_missed = missed;
}

int Player::rank() const
{
    return m_rank;
}

void Player::setRank(const int &rank)
{
    m_rank = rank;
}

bool Player::won() const
{
    return m_won;
}

void Player::setWon(const bool &won)
{
    m_won = won;
}
