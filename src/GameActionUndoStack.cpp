/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GameActionUndoStack.h"

GameActionUndoStack::GameActionUndoStack(QObject *parent) :
    QObject(parent), m_index(-1)
{
}

bool GameActionUndoStack::canUndo() const
{
    return m_index > -1;
}

bool GameActionUndoStack::canRedo() const
{
    return m_index < (m_stack.count() - 1);
}

void GameActionUndoStack::push(GameActionUndoCommand *t)
{
    while(m_index < m_stack.count()-1 && !m_stack.isEmpty())
        delete m_stack.takeLast();
    m_stack.push(t);
    m_index++;
    emit canRedoChanged();
    if(m_index == 0)
        emit canUndoChanged();
}

void GameActionUndoStack::clean()
{
    while(!m_stack.isEmpty())
    {
        delete m_stack.takeLast();
    }
    m_index = -1;
    emit canUndoChanged();
    emit canRedoChanged();
}

void GameActionUndoStack::undo()
{
    if(canUndo())
    {
        m_stack.at(m_index)->undo();
        m_index --;
        if(m_index == -1)
            emit canUndoChanged();
        if(m_index == m_stack.count()-2)
            emit canRedoChanged();
    }
}

void GameActionUndoStack::redo()
{
    if(canRedo())
    {
        m_stack.at(m_index+1)->redo();
        m_index ++;
        if(m_index == 0)
            emit canUndoChanged();
        if(m_index == m_stack.count()-1)
            emit canRedoChanged();
    }
}
