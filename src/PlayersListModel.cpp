/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PlayersListModel.h"
#include <algorithm>
#include <QDebug>

PlayersListModel::PlayersListModel(QObject *parent) :
    QAbstractListModel(parent)
{
    m_roleNames[Name] = "name";
    m_roleNames[Score] = "score";
    m_roleNames[Missed] = "missed";
    m_roleNames[Rank] = "rank";
    m_roleNames[Won] = "won";
}

void PlayersListModel::addPlayer()
{
    addPlayer(Player(tr("Player %1").arg(m_players.count()+1)));
}

void PlayersListModel::addPlayer(const Player &player)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_players << player;
    endInsertRows();
}

void PlayersListModel::clear()
{
    QModelIndex index;
    removeRows(0, rowCount(), index);
}

void PlayersListModel::shuffle()
{
    beginResetModel();

    std::random_shuffle(m_players.begin(), m_players.end());

    endResetModel();
}

int PlayersListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_players.count();
}

bool PlayersListModel::removeRows(int row, int count, QModelIndex &parent)
{
    Q_UNUSED(parent)
    beginRemoveRows(QModelIndex(), row, row+count-1);
    QList<Player>::iterator start = m_players.begin()+row;
    m_players.erase(start, start+count);
    endRemoveRows();
    return true;
}

QVariant PlayersListModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_players.count())
        return QVariant();

    const Player &player = m_players[index.row()];
    if (role == Name)
        return player.name();
    else if (role == Score)
        return player.score();
    else if (role == Missed)
        return player.missed();
    else if (role == Rank)
        return player.rank();
    else if (role == Won)
        return player.won();
    return QVariant();
}

bool PlayersListModel::setData(QModelIndex &index, QVariant &value, int role)
{
    if (index.row() < 0 || index.row() >= m_players.count())
        return false;

    Player &player = m_players[index.row()];
    if (role == Name)
    {
        player.setName(value.toString());
        emit dataChanged(index, index);
        return true;
    }
    else if (role == Score)
    {
        player.setScore(value.toInt());
        emit dataChanged(index, index);
        return true;
    }
    else if (role == Missed)
    {
        player.setMissed(value.toInt());
        emit dataChanged(index, index);
        return true;
    }
    else if (role == Rank)
    {
        player.setRank(value.toInt());
        emit dataChanged(index, index);
        return true;
    }
    else if (role == Won)
    {
        player.setWon(value.toBool());
        emit dataChanged(index, index);
        return true;
    }
    return false;
}

Qt::ItemFlags PlayersListModel::flags(QModelIndex &index) const
{
    return (QAbstractListModel::flags(index) | Qt::ItemIsEditable);
}

QHash<int, QByteArray> PlayersListModel::roleNames() const
{
    return m_roleNames;
}
