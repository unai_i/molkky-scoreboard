/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GameActionUndoCommand.h"
#include "MolkkyModel.h"
#include "PlayerGameAction.h"
#include "WinLostGameAction.h"

GameActionUndoCommand::GameActionUndoCommand(MolkkyModel *model) :
    m_model(model)
{
}

GameActionUndoCommand::~GameActionUndoCommand()
{
    while (!m_changes.isEmpty()) {
        delete m_changes.takeLast();
    }
}

void GameActionUndoCommand::undo() const
{
    foreach(GameAction *action, m_changes)
    {
        if(action->type() == GameAction::PlayerAction)
        {
            PlayerGameAction *l_action = dynamic_cast<PlayerGameAction*>(action);
            QModelIndex index = l_action->index();
            QVariant scoreVariant(l_action->before().score());
            QVariant missedVariant(l_action->before().missed());
            QVariant rankVariant(l_action->before().rank());
            QVariant wonVariant(l_action->before().won());
            m_model->m_players.setData(index, scoreVariant, PlayersListModel::Score);
            m_model->m_players.setData(index, missedVariant, PlayersListModel::Missed);
            m_model->m_players.setData(index, rankVariant, PlayersListModel::Rank);
            m_model->m_players.setData(index, wonVariant, PlayersListModel::Won);
        }
        else if(action->type() == GameAction::WinLostAction)
        {
            WinLostGameAction *l_action = dynamic_cast<WinLostGameAction*>(action);
            m_model->m_nextWinnerRank = l_action->before().nextwinRank;
            m_model->m_nextLoserRank = l_action->before().nextLostRank;
        }
    }

    m_model->m_currentPlayer = m_playerIndices.first;
    emit m_model->currentPlayerChanged();
}

void GameActionUndoCommand::redo() const
{
    foreach(GameAction *action, m_changes)
    {
        if(action->type() == GameAction::PlayerAction)
        {
            PlayerGameAction *l_action = dynamic_cast<PlayerGameAction*>(action);
            QModelIndex index = l_action->index();
            QVariant scoreVariant(l_action->after().score());
            QVariant missedVariant(l_action->after().missed());
            QVariant rankVariant(l_action->after().rank());
            QVariant wonVariant(l_action->after().won());
            m_model->m_players.setData(index, scoreVariant, PlayersListModel::Score);
            m_model->m_players.setData(index, missedVariant, PlayersListModel::Missed);
            m_model->m_players.setData(index, rankVariant, PlayersListModel::Rank);
            m_model->m_players.setData(index, wonVariant, PlayersListModel::Won);
        }
        else if(action->type() == GameAction::WinLostAction)
        {
            WinLostGameAction *l_action = dynamic_cast<WinLostGameAction*>(action);
            m_model->m_nextWinnerRank = l_action->after().nextwinRank;
            m_model->m_nextLoserRank = l_action->after().nextLostRank;
        }
    }

    m_model->m_currentPlayer = m_playerIndices.second;
    emit m_model->currentPlayerChanged();
}

void GameActionUndoCommand::setPlayerBefore(QModelIndex beforeIndex)
{
    m_playerIndices.first = beforeIndex;
}

void GameActionUndoCommand::setPlayerAfter(QModelIndex afterIndex)
{
    m_playerIndices.second = afterIndex;
}

void GameActionUndoCommand::addGameAction(GameAction *action)
{
    m_changes.append(action);
}
