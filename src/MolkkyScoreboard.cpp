/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif
#include <QtQml>

#include <sailfishapp.h>
#include <QGuiApplication>
#include <QQuickView>
#include "MolkkyModel.h"
#include <QString>

static QObject *molkkyModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    MolkkyModel *model = new MolkkyModel();
    return model;
}

int main(int argc, char *argv[])
{
    // SailfishApp::main() will display "qml/template.qml", if you need more
    // control over initialization, you can use:
    //
    //   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
    //   - SailfishApp::createView() to get a new QQuickView * instance
    //   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
    //
    // To display the view, call "show()" (will show fullscreen on device).

    QScopedPointer<QGuiApplication> app(SailfishApp::application(argc, argv));

#ifdef SAILFISHAPP_EXPORT
    app->setOrganizationName("harbour-molkkyscoreboard");
    app->setApplicationName("harbour-molkkyscoreboard");
#else
    app->setOrganizationName("Unai IRIGOYEN");
    app->setApplicationName("MolkkyScoreboard");
#endif
    app->setOrganizationDomain("u-irigoyen.com");
    QTranslator translator;
    QLocale locale = QLocale::system();
    bool ok = translator.load(locale, "molkkyscoreboard", ".", SailfishApp::pathTo("localization").path(), ".qm");
    if(!ok)
        qDebug() << "Could not set translation for locale" << locale.name() << "...";
    app->installTranslator(&translator);

    QScopedPointer<QQuickView> view(SailfishApp::createView());

    QObject::connect(view->engine(), SIGNAL(quit()), app.data(), SLOT(quit()));

    qmlRegisterSingletonType<MolkkyModel>("harbour.molkkyscoreboard.MolkkyScoreboard", 1, 0, "MolkkyModel", molkkyModelProvider);
    qmlRegisterInterface<PlayersListModel>("PlayerListModel");
    qmlRegisterInterface<QSortFilterProxyModel>("QSortFilterProxyModel");

    view->setSource(SailfishApp::pathTo("qml/harbour-molkkyscoreboard.qml"));
    view->show();

    return app->exec();
}

