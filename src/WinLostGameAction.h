/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WINLOSTGAMEACTION_H
#define WINLOSTGAMEACTION_H
#include "GameAction.h"

class WinLostGameAction : public GameAction
{
public:
    typedef struct{
        int nextwinRank;
        int nextLostRank;
    } WinLostState;

    WinLostGameAction(WinLostState before, WinLostState after);

    WinLostState before() const;
    WinLostState after() const;

private:
    QPair<WinLostState, WinLostState> m_states;
};

#endif // WINLOSTGAMEACTION_H
