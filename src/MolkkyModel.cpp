/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "MolkkyModel.h"
#include <QSettings>
#include <QStandardPaths>
#include <QCoreApplication>
#include <QDir>
#include <QDebug>
#include "PlayerGameAction.h"
#include "WinLostGameAction.h"

MolkkyModel::MolkkyModel(QObject *parent) :
    QObject(parent), m_shufflePlayers(false)
{
    m_sortedPlayers.setSourceModel(&m_players);

    m_gameVariants.insert(Official, tr("Official"));
    m_gameVariants.insert(Permissive, tr("Permissive"));
    m_gameVariants.insert(Machiavellian, tr("Machiavellian"));

    connect(&m_undoStack, SIGNAL(canUndoChanged()), this, SIGNAL(canUndoChanged()));
    connect(&m_undoStack, SIGNAL(canRedoChanged()), this, SIGNAL(canRedoChanged()));

    // create states
    m_setup = new QState();
    m_skittlePlacement = new QState();
    m_game = new QState();
    m_finished = new QState();

    // init state machine
    m_gamePhases.addState(m_setup);
    m_setup->assignProperty(this, "gamePhase", Setup);
    connect(m_setup, SIGNAL(entered()), this, SLOT(resetGame()));
    m_setup->addTransition(this, SIGNAL(setupFinished()), m_skittlePlacement);

    m_gamePhases.addState(m_skittlePlacement);
    connect(m_skittlePlacement, SIGNAL(entered()), this, SLOT(shufflePlayersIfRequested()));
    connect(m_skittlePlacement, SIGNAL(entered()), this, SLOT(resetGame()));
    m_skittlePlacement->assignProperty(this, "gamePhase", SkittlePlacement);
    m_skittlePlacement->addTransition(this, SIGNAL(placementFinished()), m_game);

    m_gamePhases.addState(m_game);
    m_game->assignProperty(this, "gamePhase", Game);
    connect(m_game, SIGNAL(entered()), this, SLOT(cleanUndoStack()));
    connect(m_game, SIGNAL(entered()), this, SLOT(setSortingAndFiltering()));
    m_game->addTransition(this, SIGNAL(gameFinished()), m_finished);
    m_game->addTransition(this, SIGNAL(startNewGame()), m_skittlePlacement);
    m_game->addTransition(this, SIGNAL(resetNewGame()), m_setup);

    m_gamePhases.addState(m_finished);
    m_finished->assignProperty(this, "gamePhase", Finished);
    connect(m_finished, SIGNAL(entered()), this, SLOT(setSortingAndFiltering()));
    m_finished->addTransition(this, SIGNAL(startNewGame()), m_skittlePlacement);
    m_finished->addTransition(this, SIGNAL(resetNewGame()), m_setup);

    m_gamePhases.setInitialState(m_setup);

    // start state machine
    m_gamePhases.start();
}

MolkkyModel::~MolkkyModel()
{
    m_gamePhases.stop();
    if(m_setup)
     delete m_setup;
    if(m_skittlePlacement)
     delete m_skittlePlacement;
    if(m_game)
     delete m_game;
    if(m_finished)
     delete m_finished;
}

MolkkyModel::GamePhase MolkkyModel::gamePhase() const
{
    return m_gamePhase;
}

void MolkkyModel::setGamePhase(MolkkyModel::GamePhase phase)
{
    if(phase != m_gamePhase)
    {
        m_gamePhase = phase;
        emit gamePhaseChanged();
    }
}

PlayersListModel* MolkkyModel::players()
{
    return &m_players;
}

QSortFilterProxyModel *MolkkyModel::sortedPlayers()
{
    return &m_sortedPlayers;
}

bool MolkkyModel::canUndo() const
{
    return m_undoStack.canUndo();
}

bool MolkkyModel::canRedo() const
{
    return m_undoStack.canRedo();
}

QStringList MolkkyModel::gameVariants() const
{
    return m_gameVariants.values();
}

QString MolkkyModel::gameVariantStr() const
{
    return m_gameVariants.value(gameVariant(), "Official");
}

MolkkyModel::GameVariant MolkkyModel::gameVariant() const
{
#ifdef ANDROID
    QSettings settings("assets:/settings.ini", QSettings::NativeFormat); //can be IniFormat, no difference
#else
    QSettings settings;
#endif
    return (GameVariant) settings.value("gameVariant", Official).toInt();
}

void MolkkyModel::setGameVariant(QString variant)
{
#ifdef ANDROID
    QSettings settings("assets:/settings.ini", QSettings::NativeFormat); //can be IniFormat, no difference
#else
    QSettings settings;
#endif
    settings.setValue("gameVariant", m_gameVariants.key(variant, Official));
#ifdef ANDROID
    settings.sync();
#endif
}

bool MolkkyModel::shufflePlayers()
{
    return m_shufflePlayers;
}

void MolkkyModel::setShufflePlayers(bool shuffle)
{
    if(shuffle != m_shufflePlayers)
    {
        m_shufflePlayers = shuffle;
        emit shufflePlayersChanged();
    }
}

int MolkkyModel::targetScore() const
{

#ifdef ANDROID
    QSettings settings("assets:/settings.ini", QSettings::NativeFormat); //can be IniFormat, no difference
#else
    QSettings settings;
#endif
    return settings.value("targetScore", 50).toInt();
}

void MolkkyModel::setTargetScore(int targetScore)
{
#ifdef ANDROID
    QSettings settings("assets:/settings.ini", QSettings::NativeFormat); //can be IniFormat, no difference
#else
    QSettings settings;
#endif
    settings.setValue("targetScore", targetScore);
#ifdef ANDROID
    settings.sync();
#endif
}

int MolkkyModel::currentPlayerIndex()
{
    return m_sortedPlayers.mapFromSource(m_currentPlayer).row();
}

QString MolkkyModel::currentPlayerName() const
{
    return m_players.data(m_currentPlayer, PlayersListModel::Name).toString();
}

int MolkkyModel::currentPlayerScore() const
{
    return m_players.data(m_currentPlayer, PlayersListModel::Score).toInt();
}

int MolkkyModel::currentPlayerMissed() const
{
    return m_players.data(m_currentPlayer, PlayersListModel::Missed).toInt();
}

QString MolkkyModel::winnerName()
{
    QModelIndexList list = m_players.match(m_players.index(0), PlayersListModel::Rank, QVariant(1), 1, Qt::MatchExactly);
    if(list.isEmpty())
        return "";
    return m_players.data(list.first(), PlayersListModel::Name).toString();
}

void MolkkyModel::setPlayerCount(int count)
{
    while(m_players.rowCount() < count)
        m_players.addPlayer();

    if (m_players.rowCount() > count) {
        QModelIndex index;
        m_players.removeRows(count, m_players.rowCount()-count, index);
    }
}

void MolkkyModel::setPlayerName(int index, const QString &name)
{
    QModelIndex modelIndex = m_players.index(index, 0);
    QVariant nameVariant = QVariant(name);
    m_players.setData(modelIndex, nameVariant, PlayersListModel::Name);
}

void MolkkyModel::resetGame()
{
    if(m_gamePhases.configuration().contains(m_setup))
    {
        // clear players and restore default
        m_players.clear();
        m_players.addPlayer(); // add player 1
        m_players.addPlayer(); // add player 2
    }
    else
    {
        // just reset scores and start new game
        int count = m_players.rowCount();
        for(int i = 0; i < count; i++)
        {
            QModelIndex index = m_players.index(i, 0);
            QVariant zeroVariant = QVariant(0);
            QVariant falseVariant = QVariant(false);
            m_players.setData(index, zeroVariant, PlayersListModel::Score);
            m_players.setData(index, zeroVariant, PlayersListModel::Missed);
            m_players.setData(index, zeroVariant, PlayersListModel::Rank);
            m_players.setData(index, falseVariant, PlayersListModel::Won);
        }
        m_currentPlayer = m_players.index(0);
        m_nextWinnerRank = 1;
        m_nextLoserRank = m_players.rowCount();
    }

    emit winnerNameChanged();
}

void MolkkyModel::setSortingAndFiltering()
{
    if(m_gamePhases.configuration().contains(m_game))
    {
        m_sortedPlayers.setFilterRole(PlayersListModel::Rank);
        m_sortedPlayers.setFilterFixedString("0");
        m_sortedPlayers.setSortRole(PlayersListModel::Score);
        m_sortedPlayers.sort(0, Qt::DescendingOrder);
    }
    else if(m_gamePhases.configuration().contains(m_finished))
    {
        m_sortedPlayers.setFilterRole(PlayersListModel::Rank);
        m_sortedPlayers.setFilterFixedString("");
        m_sortedPlayers.setSortRole(PlayersListModel::Rank);
        m_sortedPlayers.sort(0, Qt::AscendingOrder);
    }
}

void MolkkyModel::cleanUndoStack()
{
    m_undoStack.clean();
}

void MolkkyModel::markPlayerScore(int points)
{
    GameVariant variant = gameVariant();
    int targetScore = this->targetScore();

    GameActionUndoCommand *undoCommand = new GameActionUndoCommand(this);
    undoCommand->setPlayerBefore(m_currentPlayer);

    // record player state before
    Player currentPlayerBefore;
    currentPlayerBefore.setName(m_players.data(m_currentPlayer, PlayersListModel::Name).toString());
    currentPlayerBefore.setScore(m_players.data(m_currentPlayer, PlayersListModel::Score).toInt());
    currentPlayerBefore.setMissed(m_players.data(m_currentPlayer, PlayersListModel::Missed).toInt());
    currentPlayerBefore.setRank(m_players.data(m_currentPlayer, PlayersListModel::Rank).toInt());
    currentPlayerBefore.setWon(m_players.data(m_currentPlayer, PlayersListModel::Won).toBool());

    if(points > 0)
    {
        // add points and reset missed
        QVariant score(m_players.data(m_currentPlayer, PlayersListModel::Score).toInt() + points);
        QVariant zeroVariant(0);
        m_players.setData(m_currentPlayer, score, PlayersListModel::Score);
        m_players.setData(m_currentPlayer, zeroVariant, PlayersListModel::Missed);

        if(variant == Machiavellian)
        {
            QModelIndexList list = m_players.match(m_players.index(0),
                                                   PlayersListModel::Score, score,
                                                   -1, Qt::MatchExactly | Qt::MatchWrap);
            foreach (QModelIndex index, list) {
                if(index != m_currentPlayer)
                {
                    QVariant newScore(m_players.data(index, PlayersListModel::Score).toInt() <= targetScore/2?
                                          0 : targetScore/2);

                    // record player state before
                    Player playerBefore;
                    playerBefore.setName(m_players.data(index, PlayersListModel::Name).toString());
                    playerBefore.setScore(m_players.data(index, PlayersListModel::Score).toInt());
                    playerBefore.setMissed(m_players.data(index, PlayersListModel::Missed).toInt());
                    playerBefore.setRank(m_players.data(index, PlayersListModel::Rank).toInt());
                    playerBefore.setWon(m_players.data(index, PlayersListModel::Won).toBool());

                    m_players.setData(index, newScore, PlayersListModel::Score);

                    // record player state after
                    Player playerAfter;
                    playerAfter.setName(playerBefore.name());
                    playerAfter.setScore(m_players.data(index, PlayersListModel::Score).toInt());
                    playerAfter.setMissed(playerBefore.missed());
                    playerAfter.setRank(playerBefore.rank());
                    playerAfter.setWon(playerBefore.won());

                    undoCommand->addGameAction(new PlayerGameAction(index, playerBefore, playerAfter));
                }
            }
        }

        if(score.toInt() == targetScore)
        {
            // we have a winner
            QVariant rank(m_nextWinnerRank);
            QVariant won(true);
            m_players.setData(m_currentPlayer, rank, PlayersListModel::Rank);
            if(m_nextWinnerRank == 1)
                emit winnerNameChanged();
            WinLostGameAction::WinLostState before;
            before.nextwinRank = m_nextWinnerRank;
            before.nextLostRank = m_nextLoserRank;
            m_nextWinnerRank++;
            WinLostGameAction::WinLostState after;
            after.nextwinRank = m_nextWinnerRank;
            after.nextLostRank = m_nextLoserRank;
            undoCommand->addGameAction(new WinLostGameAction(before, after));
            m_players.setData(m_currentPlayer, won, PlayersListModel::Won);
            emit gamePlayerCountChanged(m_players.data(m_currentPlayer, PlayersListModel::Name).toString(), true);
            if(m_nextLoserRank == m_nextWinnerRank)
            {
                // if there is one last team left
                QModelIndexList matches = m_players.match(m_players.index(0),
                                                          PlayersListModel::Rank,
                                                          QVariant(0),
                                                          -1, Qt::MatchExactly);

                if(matches.count())
                {
                    Q_ASSERT(matches.count() == 1);
                    QVariant rank(m_nextLoserRank);
                    m_players.setData(matches.first(), rank, PlayersListModel::Rank);
                }
                emit gameFinished();
                return;
            }
        }
        else if(score.toInt() > targetScore)
        {
            // team has set too much points
            QVariant newScore(targetScore/2);
            m_players.setData(m_currentPlayer, newScore, PlayersListModel::Score);
        }
    }
    else
    {
        QVariant missed(m_players.data(m_currentPlayer, PlayersListModel::Missed).toInt()+1);
        m_players.setData(m_currentPlayer, missed, PlayersListModel::Missed);
        if(missed.toInt() == 3)
        {
            if(variant == Official)
            {
                // mark current team as loser
                QVariant rank(m_nextLoserRank);
                m_players.setData(m_currentPlayer, rank, PlayersListModel::Rank);
                WinLostGameAction::WinLostState before;
                before.nextwinRank = m_nextWinnerRank;
                before.nextLostRank = m_nextLoserRank;
                m_nextLoserRank--;
                WinLostGameAction::WinLostState after;
                after.nextwinRank = m_nextWinnerRank;
                after.nextLostRank = m_nextLoserRank;
                undoCommand->addGameAction(new WinLostGameAction(before, after));
                emit gamePlayerCountChanged(m_players.data(m_currentPlayer, PlayersListModel::Name).toString(), false);
                if(m_nextLoserRank == m_nextWinnerRank)
                {
                    // if there is one last team left
                    QModelIndexList matches = m_players.match(m_players.index(0),
                                                              PlayersListModel::Rank,
                                                              QVariant(0),
                                                              -1, Qt::MatchExactly);

                    if(matches.count())
                    {
                        Q_ASSERT(matches.count() == 1);
                        QVariant rank(m_nextWinnerRank);
                        QVariant won(true);
                        m_players.setData(matches.first(), rank, PlayersListModel::Rank);
                        m_players.setData(matches.first(), won, PlayersListModel::Won);
                    }
                    emit gameFinished();
                    return;
                }
            }
            else {
                QVariant newScore(m_players.data(m_currentPlayer, PlayersListModel::Score).toInt() <= targetScore/2?
                                      0 : targetScore/2);
                QVariant zeroVariant(0);
                m_players.setData(m_currentPlayer, newScore, PlayersListModel::Score);
                m_players.setData(m_currentPlayer, zeroVariant, PlayersListModel::Missed);
            }
        }
    }

    // record player state after
    Player currentPlayerAfter;
    currentPlayerAfter.setName(m_players.data(m_currentPlayer, PlayersListModel::Name).toString());
    currentPlayerAfter.setScore(m_players.data(m_currentPlayer, PlayersListModel::Score).toInt());
    currentPlayerAfter.setMissed(m_players.data(m_currentPlayer, PlayersListModel::Missed).toInt());
    currentPlayerAfter.setRank(m_players.data(m_currentPlayer, PlayersListModel::Rank).toInt());
    currentPlayerAfter.setWon(m_players.data(m_currentPlayer, PlayersListModel::Won).toBool());

    // add state change to command
    undoCommand->addGameAction(new PlayerGameAction(m_currentPlayer, currentPlayerBefore, currentPlayerAfter));

    QModelIndex start;
    if(m_currentPlayer.row() < m_players.rowCount()-1)
        start = m_currentPlayer.sibling(m_currentPlayer.row()+1, 0);
    else
        start = m_players.index(0);
    QModelIndexList matches = m_players.match(start,
                                              PlayersListModel::Rank,
                                              QVariant(0),
                                              1, Qt::MatchExactly | Qt::MatchWrap);

    m_currentPlayer = matches.first();
    emit currentPlayerChanged();

    undoCommand->setPlayerAfter(m_currentPlayer);
    m_undoStack.push(undoCommand);
}

void MolkkyModel::shufflePlayersIfRequested()
{
    if(m_shufflePlayers)
        m_players.shuffle();
}

void MolkkyModel::undo()
{
    m_undoStack.undo();
}

void MolkkyModel::redo()
{
    m_undoStack.redo();
}
