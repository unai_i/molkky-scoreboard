/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PLAYERGAMEACTION_H
#define PLAYERGAMEACTION_H
#include "GameAction.h"

class PlayerGameAction : public GameAction
{
public:
    PlayerGameAction(QModelIndex index, Player before, Player after);

    QModelIndex index() const;
    Player before() const;
    Player after() const;

private:
    QModelIndex m_index;
    QPair<Player, Player> m_states;
};

#endif // PLAYERGAMEACTION_H
