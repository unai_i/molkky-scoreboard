/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MOLKKYMODEL_H
#define MOLKKYMODEL_H

#include <QObject>
#include <QStateMachine>
#include <QSortFilterProxyModel>
#include <QStringList>
#include "PlayersListModel.h"
#include "GameActionUndoStack.h"

class MolkkyModel : public QObject
{
    Q_OBJECT
    friend class GameActionUndoCommand;

    Q_PROPERTY(GamePhase gamePhase READ gamePhase WRITE setGamePhase NOTIFY gamePhaseChanged)
    Q_PROPERTY(PlayersListModel* players READ players CONSTANT)
    Q_PROPERTY(QSortFilterProxyModel* sortedPlayers READ sortedPlayers CONSTANT)
    Q_PROPERTY(QStringList gameVariants READ gameVariants CONSTANT)
    Q_PROPERTY(QString gameVariant READ gameVariantStr WRITE setGameVariant)
    Q_PROPERTY(int targetScore READ targetScore WRITE setTargetScore)
    Q_PROPERTY(bool shufflePlayers READ shufflePlayers WRITE setShufflePlayers NOTIFY shufflePlayersChanged)

    Q_PROPERTY(int currentPlayerIndex READ currentPlayerIndex NOTIFY currentPlayerChanged)
    Q_PROPERTY(QString currentPlayerName READ currentPlayerName NOTIFY currentPlayerChanged)
    Q_PROPERTY(int currentPlayerScore READ currentPlayerScore NOTIFY currentPlayerChanged)
    Q_PROPERTY(int currentPlayerMissed READ currentPlayerMissed NOTIFY currentPlayerChanged)

    Q_PROPERTY(bool canUndo READ canUndo NOTIFY canUndoChanged)
    Q_PROPERTY(bool canRedo READ canRedo NOTIFY canRedoChanged)

    Q_PROPERTY(QString winnerName READ winnerName NOTIFY winnerNameChanged)

    Q_ENUMS(GamePhase)

public:

    enum GamePhase
    {
        Setup,
        SkittlePlacement,
        Game,
        Finished
    };

    enum GameVariant
    {
        Official,
        Permissive,
        Machiavellian
    };

    explicit MolkkyModel(QObject *parent = 0);
    ~MolkkyModel();

    GamePhase gamePhase() const;
    void setGamePhase(GamePhase phase);

    PlayersListModel *players();
    QSortFilterProxyModel* sortedPlayers();

    bool canUndo() const;
    bool canRedo() const;

    QStringList gameVariants() const;

    QString gameVariantStr() const;
    GameVariant gameVariant() const;
    void setGameVariant(QString variant);

    bool shufflePlayers();
    void setShufflePlayers(bool shuffle);

    int targetScore() const;
    void setTargetScore(int targetScore);

    int currentPlayerIndex();
    QString currentPlayerName() const;
    int currentPlayerScore() const;
    int currentPlayerMissed() const;

    QString winnerName();

    
signals:
    void gamePhaseChanged();
    void setupFinished();
    void placementFinished();
    void gameFinished();
    void startNewGame();
    void resetNewGame();

    void shufflePlayersChanged();

    void currentPlayerChanged();
    void gamePlayerCountChanged(QString name, bool won);
    void winnerNameChanged();

    void canUndoChanged();
    void canRedoChanged();
    
public slots:
    void setPlayerCount(int count);
    void setPlayerName(int index, const QString &name);
    void markPlayerScore(int points);

    void undo();
    void redo();

private slots:
    void resetGame();
    void setSortingAndFiltering();
    void cleanUndoStack();

    void shufflePlayersIfRequested();

private:
    GamePhase m_gamePhase;
    PlayersListModel m_players;
    QSortFilterProxyModel m_sortedPlayers;

    GameActionUndoStack m_undoStack;

    QMap<GameVariant, QString> m_gameVariants;

    bool m_shufflePlayers;

    // game parameters
    QModelIndex m_currentPlayer;
    int m_nextWinnerRank;
    int m_nextLoserRank;

    // state machine
    QStateMachine m_gamePhases;
    // states
    QState *m_setup;
    QState *m_skittlePlacement;
    QState *m_game;
    QState *m_finished;
    
};

#endif // MOLKKYMODEL_H
