/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GAMEACTIONUNDOCOMMAND_H
#define GAMEACTIONUNDOCOMMAND_H

#include "GameAction.h"

class MolkkyModel;
class GameActionUndoCommand
{
public:
    explicit GameActionUndoCommand(MolkkyModel *model = NULL);
    ~GameActionUndoCommand();

    void undo() const;
    void redo() const;

    void setPlayerBefore(QModelIndex beforeIndex);
    void setPlayerAfter(QModelIndex afterIndex);
    void addGameAction(GameAction *action);

private:
    MolkkyModel *m_model;
    QPair<QModelIndex, QModelIndex> m_playerIndices;
    QList<GameAction*> m_changes;

};

#endif // GAMEACTIONUNDOCOMMAND_H
