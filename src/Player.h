/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PLAYER_H
#define PLAYER_H

#include <QString>

class Player
{
public:
    Player(QString name = "");

    QString name() const;
    void setName(const QString &name);

    int score() const;
    void setScore(const int &score);

    int missed() const;
    void setMissed(const int &missed);

    int rank() const;
    void setRank(const int &rank);

    bool won() const;
    void setWon(const bool &won);

private:
    QString m_name;
    int m_score;
    int m_missed;
    int m_rank;
    bool m_won;
};

#endif // PLAYER_H
