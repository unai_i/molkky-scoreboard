/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GAMEACTIONUNDOSTACK_H
#define GAMEACTIONUNDOSTACK_H

#include <QObject>
#include <QStack>
#include "GameActionUndoCommand.h"

class GameActionUndoStack : public QObject
{
    Q_OBJECT

public:
    explicit GameActionUndoStack(QObject *parent = 0);

    bool canUndo() const;
    bool canRedo() const;

    void push(GameActionUndoCommand *t);
    void clean();

signals:
    void canUndoChanged();
    void canRedoChanged();

public slots:
    void undo();
    void redo();

private:
    QStack<GameActionUndoCommand*> m_stack;
    int m_index;
};

#endif // GAMEACTIONUNDOSTACK_H
