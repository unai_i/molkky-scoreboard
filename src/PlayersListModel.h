/* Copyright Unai IRIGOYEN 2013
 * All rights reserved.
 *
 * This file is part of Molkky Scoreboard.
 *
 * Molkky Scoreboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Molkky Scoreboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Molkky Scoreboard.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PLAYERSLISTMODEL_H
#define PLAYERSLISTMODEL_H

#include <QAbstractListModel>
#include "Player.h"

class PlayersListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum PlayerRoles{
        Name = Qt::UserRole+1,
        Score,
        Missed,
        Rank,
        Won
    };

    explicit PlayersListModel(QObject *parent = 0);

    void addPlayer();
    void addPlayer(const Player &player);

    void clear();
    void shuffle();

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    bool removeRows(int row, int count, QModelIndex &parent);

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    bool setData(QModelIndex &index, QVariant &value, int role);

    Qt::ItemFlags flags(QModelIndex &index) const;

signals:

public slots:

protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<Player> m_players;

    QHash<int, QByteArray> m_roleNames;
};

#endif // PLAYERSLISTMODEL_H
