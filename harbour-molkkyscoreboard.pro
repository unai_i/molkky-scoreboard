# The name of your app.
# NOTICE: name defined in TARGET has a corresponding QML filename.
#         If name defined in TARGET is changed, following needs to be
#         done to match new name:
#         - corresponding QML filename must be changed
#         - desktop icon filename must be changed
#         - desktop filename must be changed
#         - icon definition filename in desktop file must be changed
TARGET = harbour-molkkyscoreboard

CONFIG += sailfishapp

localization.files = localization
localization.path = /usr/share/$${TARGET}
INSTALLS += localization

lupdate_only {
    SOURCES = qml/*.qml \
              qml/pages/*.qml \
              qml/cover/*.qml
    TRANSLATIONS = localization/molkkyscoreboard.fr.ts \
                   localization/molkkyscoreboard.es.ts
}

SOURCES += src/MolkkyScoreboard.cpp \
    src/MolkkyModel.cpp \
    src/PlayersListModel.cpp \
    src/Player.cpp \
    src/GameActionUndoCommand.cpp \
    src/GameActionUndoStack.cpp \
    src/GameAction.cpp \
    src/PlayerGameAction.cpp \
    src/WinLostGameAction.cpp

OTHER_FILES += \
    qml/cover/CoverPage.qml \
    rpm/harbour-molkkyscoreboard.spec \
    qml/pages/SetupPage.qml \
    qml/pages/GamePage.qml \
    qml/pages/SkittlePlacementPage.qml \
    qml/pages/SettingsPage.qml \
    harbour-molkkyscoreboard.desktop \
    rpm/harbour-molkkyscoreboard.yaml \
    qml/harbour-molkkyscoreboard.qml \
    qml/pages/FinishedPage.qml

HEADERS += \
    src/MolkkyModel.h \
    src/PlayersListModel.h \
    src/Player.h \
    src/GameActionUndoCommand.h \
    src/GameActionUndoStack.h \
    src/GameAction.h \
    src/PlayerGameAction.h \
    src/WinLostGameAction.h
